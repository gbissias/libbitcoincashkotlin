import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    kotlin("plugin.serialization")
    `maven-publish`
}

android {
    compileSdkVersion(29)

    defaultConfig {
        testApplicationId = "info.bitcoinunlimited.libbitcoincash"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        setTestFunctionalTest(true)

        minSdkVersion(26)
        targetSdkVersion(29)

        consumerProguardFiles("proguard-rules.pro")

        versionCode = 1
        versionName = "0.4.1"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        externalNativeBuild {
            cmake {
                cppFlags.add("-std=c++14")
                targets.add("bitcoincashandroid")
                arguments.add("-DANDROID_STL=c++_shared")
                arguments.add("-DANDROID")
            }
        }

        ndk {
            abiFilters("x86", "x86_64", "armeabi-v7a", "arm64-v8a")
        }

        ndkVersion = "21.1.6352462"
    }

    buildTypes {
        getByName("debug")
        getByName("release") {
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
        kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    externalNativeBuild {
        cmake {
            setPath(file("./src/main/cpp/CMakeLists.txt"))
            setVersion("3.11.0+")
        }
    }

    packagingOptions {
        exclude("META-INF/*md")
    }

    sourceSets {
        all {
            java.srcDir("src/$name/kotlin")
        }
    }
}

dependencies {
    val junitVersion = "5.6.2"
    val roomVersion = "2.2.5"
    implementation("androidx.room", "room-runtime", roomVersion)
    implementation(kotlin("reflect"))
    implementation(kotlin("stdlib-jdk8"))

    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.3.9")
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-cbor", "1.0.0-RC")
    kapt("androidx.room", "room-compiler", roomVersion)

    testImplementation("org.junit.jupiter", "junit-jupiter", junitVersion)

    androidTestImplementation("androidx.test", "core", "1.3.0-beta01")
    androidTestImplementation("androidx.test.ext", "junit", "1.1.2-beta01")
    androidTestImplementation("androidx.test", "runner", "1.3.0-beta01")
    androidTestImplementation("com.google.truth", "truth", "1.0")
    androidTestImplementation("org.jetbrains.kotlin", "kotlin-test")
}

tasks.withType<Test> {
    // Tests that do not require external shared library
    useJUnitPlatform()
}

fun Project.getKtlintConfiguration(): Configuration {
    return configurations.findByName("ktlint") ?: configurations.create("ktlint") {
        val dependency = project.dependencies.create("com.pinterest:ktlint:0.37.2")
        dependencies.add(dependency)
    }
}

/**
 * List of files that are linted.
 */
fun lintedFileList(): List<String> {
    return listOf(
        "build.gradle.kts",
        "src/androidTest/**/TwoOptionVoteContractTests.kt",
        "src/androidTest/**/TwoOptionVoteTests.kt",
        "src/androidTest/**/VoteTestUtil.kt",
        "src/main/**/TwoOptionVote.kt",
        "src/main/**/TwoOptionVoteContract.kt",
        "src/main/**/Key.kt",
        "src/main/**/UtilStringEncoding.kt",
        "src/test/**/*.kt"
    )
}

tasks.register("ktlint", JavaExec::class.java) {
    description = "Check Kotlin code style."
    group = "Verification"
    classpath = getKtlintConfiguration()
    main = "com.pinterest.ktlint.Main"
    args = listOf("--android") + lintedFileList()
}
tasks.register("ktlintFormat", JavaExec::class.java) {
    description = "Fix Kotlin code style deviations."
    group = "formatting"
    classpath = getKtlintConfiguration()
    main = "com.pinterest.ktlint.Main"
    args = listOf("-F", "--android") + lintedFileList()
}

// Publishing
group = "info.bitcoinunlimited"
version = "0.2.3"

publishing {
    publications {
        create<MavenPublication>("libbitcoincash") {
            // from(components["java"])
            artifact("$buildDir/outputs/aar/libbitcoincash-release.aar")
        }
    }

    repositories {
        maven {
            url = uri("$buildDir/repos")
        }
    }
}

tasks.map {
    if (it.name.startsWith("publish")) {
        it.dependsOn(tasks.findByName("build")!!)
    }
}
