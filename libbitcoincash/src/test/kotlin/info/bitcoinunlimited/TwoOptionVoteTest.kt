package info.bitcoinunlimited.libbitcoincash

import bitcoinunlimited.libbitcoincash.TwoOptionVote
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Test

class TwoOptionVoteTest {
    @Test
    fun test_sort_byte_array() {
        var array = arrayOf(
            "b".toByteArray(),
            "a".toByteArray()
        )
        var expected = arrayOf(
            "a".toByteArray(),
            "b".toByteArray()
        )
        assertArrayEquals(expected, TwoOptionVote.sortByteArray(array))
    }
}
