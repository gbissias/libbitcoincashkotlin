
// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

import kotlin.system.measureTimeMillis


// Note enum does not work for these script opcodes because I want the opcodes to be the same type as ByteArray so that data and opcodes can be pushed in the same parameter, list, or varargs
// without qualifying the enum with .ordinal
class OP(val v:ByteArray)
{
    companion object
    {
        val FALSE = byteArrayOf(0x0.toByte())
        val TRUE  = byteArrayOf(0x51.toByte())

        val NOP = byteArrayOf(0x61.toByte())    // control operations
        val VER = byteArrayOf(0x62.toByte())
        val IF = byteArrayOf(0x63.toByte())
        val NOTIF = byteArrayOf(0x64.toByte())
        val VERIF = byteArrayOf(0x65.toByte())
        val VERNOTIF = byteArrayOf(0x66.toByte())
        val ELSE = byteArrayOf(0x67.toByte())
        val ENDIF = byteArrayOf(0x68.toByte())
        val VERIFY = byteArrayOf(0x69.toByte())
        val RETURN = byteArrayOf(0x6a.toByte())
        val TOALTSTACK = byteArrayOf(0x6b.toByte())     // stack operations
        val FROMALTSTACK = byteArrayOf(0x6c.toByte())
        val DROP2 = byteArrayOf(0x6d.toByte())
        val DUP2 = byteArrayOf(0x6e.toByte())
        val DUP3 = byteArrayOf(0x6f.toByte())

        val OVER2 = byteArrayOf(0x70.toByte())
        val ROT2 = byteArrayOf(0x71.toByte())
        val SWAP2 = byteArrayOf(0x72.toByte())
        val IFDUP = byteArrayOf(0x73.toByte())
        val DEPTH = byteArrayOf(0x74.toByte())
        val DROP = byteArrayOf(0x75.toByte())
        val DUP = byteArrayOf(0x76.toByte())
        val NIP = byteArrayOf(0x77.toByte())
        val OVER = byteArrayOf(0x78.toByte())
        val PICK = byteArrayOf(0x79.toByte())
        val ROLL = byteArrayOf(0x7a.toByte())
        val ROT = byteArrayOf(0x7b.toByte())
        val SWAP = byteArrayOf(0x7c.toByte())
        val TUCK = byteArrayOf(0x7d.toByte())
        val CAT = byteArrayOf(0x7e.toByte())      // string operations
        val SPLIT = byteArrayOf(0x7f.toByte())
        val NUM2BIN = byteArrayOf(0x80.toByte())  // number conversion

        val BIN2NUM = byteArrayOf(0x81.toByte())
        val SIZE = byteArrayOf(0x82.toByte())
        val INVERT = byteArrayOf(0x83.toByte())
        val AND = byteArrayOf(0x84.toByte())
        val OR = byteArrayOf(0x85.toByte())
        val XOR = byteArrayOf(0x86.toByte())
        val EQUAL = byteArrayOf(0x87.toByte())
        val EQUALVERIFY = byteArrayOf(0x88.toByte())
        val RESERVED1 = byteArrayOf(0x89.toByte())
        val RESERVED2 = byteArrayOf(0x8a.toByte())
        val ADD1 = byteArrayOf(0x8b.toByte())
        val SUB1 = byteArrayOf(0x8c.toByte())
        val MUL2 = byteArrayOf(0x8d.toByte())
        val DIV2 = byteArrayOf(0x8e.toByte())
        val NEGATE = byteArrayOf(0x8f.toByte())

        val ABS = byteArrayOf(0x90.toByte())
        val NOT = byteArrayOf(0x91.toByte())
        val NOTEQUAL0 = byteArrayOf(0x92.toByte())
        val ADD = byteArrayOf(0x93.toByte())
        val SUB = byteArrayOf(0x94.toByte())
        val MUL = byteArrayOf(0x95.toByte())
        val DIV = byteArrayOf(0x96.toByte())
        val MOD = byteArrayOf(0x97.toByte())
        val LSHIFT = byteArrayOf(0x98.toByte())
        val RSHIFT = byteArrayOf(0x99.toByte())

        val HASH160  = byteArrayOf(0xa9.toByte())
        val CHECKSIG = byteArrayOf(0xac.toByte())
        val CHECKSIGVERIFY = byteArrayOf(0xad.toByte())

        val PUSHDATA1 = byteArrayOf(0x4c.toByte())
        val PUSHDATA2 = byteArrayOf(0x4d.toByte())
        val PUSHDATA4 = byteArrayOf(0x4e.toByte())


        // Not real opcodes -- used to match data types in script identification comparisons
        val TMPL_BIGINTEGER = byteArrayOf(0xf0.toByte())
        val TMPL_DATA = byteArrayOf(0xf1.toByte())
        val TMPL_SMALLINTEGER = byteArrayOf(0xfa.toByte())
        val TMPL_PUBKEYS = byteArrayOf(0xfb.toByte())
        val TMPL_PUBKEYHASH = byteArrayOf(0xfd.toByte())
        val TMPL_PUBKEY =byteArrayOf(0xfe.toByte())

        val LAST_DATAPUSH = 0x4b

        fun isPushData0(v:Byte):Int
        {
            if ((v >=1)  && (v <= 0x4b)) return v.toPositiveInt()
            return 0
        }

        fun push(data: ByteArray):ByteArray
        {
            if (data.size < LAST_DATAPUSH)
            {
                return byteArrayOf(data.size.toByte()) + data
            }
            if (data.size < 256)
                return PUSHDATA1 + data.size.toByte() + data
            if (data.size < 0x10000)
                throw UnimplementedException("Pushing more than 256 bytes of data")
            //    return PUSHDATA2 + data.size.toShort() + data
            else
                throw UnimplementedException("Pushing more data then stack allows")
            //    return PUSHDATA4 + data.size.toInt() + data
        }
    }
}


/*
enum class OP(val v:ByteArray)  // ByteArray to support future multibyte instructions
{
    DUP(byteArrayOf(0x76.toByte())),
    EQUAL(byteArrayOf(0x87.toByte())),
    EQUALVERIFY(byteArrayOf(0x88.toByte())),
    HASH160  (byteArrayOf(0xa9.toByte())),
    CHECKSIG (byteArrayOf(0xac.toByte())),
    CHECKSIGVERIFY (byteArrayOf(0xad.toByte())),

    PUSHDATA1 (byteArrayOf(0x4c.toByte())),
    PUSHDATA2 (byteArrayOf(0x4d.toByte())),
    PUSHDATA4 (byteArrayOf(0x4e.toByte())),


    // Not real opcodes -- used to match data types in script identification comparisons
    TMPL_BIGINTEGER(byteArrayOf(0xf0.toByte())),
    TMPL_DATA(byteArrayOf(0xf1.toByte())),
    TMPL_SMALLINTEGER(byteArrayOf(0xfa.toByte())),
    TMPL_PUBKEYS(byteArrayOf(0xfb.toByte())),
    TMPL_PUBKEYHASH(byteArrayOf(0xfd.toByte())),
    TMPL_PUBKEY(byteArrayOf(0xfe.toByte()))
    ;

    operator fun plus(script: BCHscript): BCHscript
    {
        var ret = script
        ret.add(v)
        return ret
    }

    companion object
    {

        fun push(data: ByteArray): ByteArray
        {
            if (data.size == 1)
            {
                if (data[0] < 0x4c.toByte()) return byteArrayOf(data[0])
            }
            if (data.size < 256)
                return PUSHDATA1.v + data
            if (data.size < 0x10000)
                return PUSHDATA2.v + data
            else
                return PUSHDATA4.v + data
        }
    }

}
*/

@cli(Display.Simple, "Scripts constrain spending an output or satisfy other scripts' constraints")
class BCHscript(val chainSelector: ChainSelector) : BCHserializable()
{
    var data = MutableList<ByteArray>(0, { _ -> ByteArray(0)})

    // Construction
    @cli(Display.User, "Construct a script from a hex string")
    constructor(chainSelector: ChainSelector, script: String):this(chainSelector)
    {
        data.add(element = script.FromHex())
    }

    @cli(Display.User, "Construct a script from raw bytes")
    constructor(chainSelector: ChainSelector, vararg instructions: ByteArray):this(chainSelector)
    {
        for (i in instructions)
        {
            data.add(i)
        }
    }

    @cli(Display.User, "Construct a script from individual instructions")
    constructor(chainSelector: ChainSelector, vararg instructions: OP):this(chainSelector)
    {
        for (i in instructions)
        {
            data.add(i.v)
        }
    }

    /** The single SHA256 hash of this script.  Used by some services as a global pointer identifying the script */
    @cli(Display.User, "The single SHA256 hash of this script.  Used by some services as a global pointer identifying the script")
    fun scriptHash():Hash256
    {
        return Hash256(Hash.sha256(BCHserialize(SerializationType.SCRIPTHASH).flatten()))
    }

    @cli(Display.User, "The RIPEMD160 of the SHA256 hash of this script.  Used by P2SH scripts")
    fun scriptHash160():ByteArray
    {
        return Hash.hash160(BCHserialize(SerializationType.SCRIPTHASH).flatten())
    }


    @cli(Display.User, "Are 2 scripts the same?")
    fun contentEquals(other: BCHscript): Boolean
    {
        val a = flatten()
        val b = other.flatten()
        return a.contentEquals(b)
    }

    @cli(Display.User, "Append raw script bytes to this script.  Note! Use  '+ OP.push(...)' to push data into a script")
    operator fun plus(rawscript: ByteArray): BCHscript
    {
        var ret = this
        ret.add(rawscript)
        return ret
    }

    // Composition
    @cli(Display.User, "Append a new instruction")
    fun add(cmd: Byte): BCHscript
    {
        data.add(ByteArray(1, {_->cmd}))
        return this
    }

    @cli(Display.User, "Append raw script bytes to this script.  Note! Use add(OP.push(...)) to push data into a script")
    fun add(cmds: ByteArray):BCHscript
    {
        data.add(cmds)
        return this
    }

    // Converts the internal representation to a single ByteArray
    @cli(Display.User, "Converts the internal representation to a single ByteArray")
    fun flatten(): ByteArray
    {
        data = MutableList<ByteArray>(1,{ _ -> data.join()})
        return data[0]
    }

    // Serialization
    @cli(Display.User, "Create a script from serialized data")
    constructor(chainSelector: ChainSelector, buf: BCHserialized):this(chainSelector)
    {
        BCHdeserialize(buf)
    }

    @cli(Display.User, "Overwrite this script with serialized data")
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        data.clear()
        var len = stream.decompact()
        data.add(stream.debytes(len))
        return stream
    }

    @cli(Display.User, "Serialize this script")
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var alldata: ByteArray = data.join()
        if (format == SerializationType.SCRIPTHASH)
        {
            return BCHserialized(alldata, format)
        }


        var ret = BCHserialized.compact(alldata.size.toLong(), format) + BCHserialized(alldata, format)
        return ret
    }

    @cli(Display.User, "Convert to serialized byte array (suitable for network, execution or hashing)")
    fun toByteArray():ByteArray
    {
        return BCHserialize(SerializationType.SCRIPTHASH).flatten()
    }


    data class MatchResult(val type: PayAddressType, val params: MutableList<ByteArray>)
    {
    }

    @cli(Display.User, "Parse common script types (P2PKH and P2SH) and return useful data")
    fun match():MatchResult?
    {
        matches(P2PKHtemplate)?.run { return MatchResult(PayAddressType.P2PKH, this) }
        matches(P2SHtemplate)?.run { return MatchResult(PayAddressType.P2SH, this) }
        return null
    }

    @cli(Display.Dev, "Attempt to match this script to a template")
    fun matches(template: BCHscript): MutableList<ByteArray>?
    {
        val script = flatten()
        val tmpl = template.flatten()
        var ret = mutableListOf<ByteArray>()

        var ti = 0 // current location in the template
        var i = 0 // current location in the script
        while ((i < script.size) && (ti < tmpl.size))
        {
            if (tmpl[ti] == script[i] ) { i+=1; ti+=1; continue }
            if (tmpl[ti] == OP.TMPL_PUBKEYHASH[0])
            {
                if (OP.isPushData0(script[i]) > 0)  // Handle the direct push case
                {
                    val datalen = script[i]
                    if (datalen != 20.toByte()) return null   // pubkeyhash must be 20 bytes
                    i+=1
                    ret.add(script.sliceArray(IntRange(i,i+19)))
                    i+=20
                    ti+=1
                    continue
                }

                if (script[i] == OP.PUSHDATA1[0]) // Handle the pushdata1 case
                {
                    i += 1
                    val datalen = script[i]
                    if (datalen != 20.toByte()) return null   // pubkeyhash must be 20 bytes
                    i+=1
                    ret.add(script.sliceArray(IntRange(i,i+19)))
                    i+=20
                    ti+=1
                    continue
                }

                // TODO the other pushdata operations
                //    PUSHDATA2
                //    PUSHDATA4
            }

            // TODO the other template items
            return null
        }

        if ((i == script.size) && (ti == tmpl.size)) return ret  // both script and template ended at the same moment, template matches
        return null
    }

    // Analysis
    @cli(Display.User, "Parse common script types (P2PKH and P2SH) and return the address or null if the script is not standard")
    val address: PayAddress? // nullable because its possible that I can't understand this script or that it is an anyone-can-pay (has no constraint)
    get()
    {
        matches(P2PKHtemplate)?.run {
            return PayAddress(chainSelector, PayAddressType.P2PKH, this[0])
        }

        matches(P2SHtemplate)?.run {
            return PayAddress(chainSelector, PayAddressType.P2SH, this[0])
        }
        return null
    }

    @cli(Display.User, "Return the P2SH constraint (output) script corresponding to this redeem script")
    fun P2SHconstraint(): BCHscript
    {
        return BCHscript(chainSelector, OP.HASH160, OP.push(scriptHash160()), OP.EQUAL)
    }

    @cli(Display.User, "Return the P2SH part of the satisfier (input) script corresponding to this redeem script.  You must subsequently append a script that actually satisfies the redeem script. ")
    fun P2SHsatisfier(): BCHscript
    {
        return BCHscript(chainSelector, OP.push(toByteArray()))
    }

    @cli(Display.Simple, "Convert to hex notation")
    fun toHex(): String
    {
        return flatten().toHex()
    }

    override fun toString(): String = "BCHscript(\"" + toHex() + "\")"

    // Execution
    companion object
    {
        // Right now, the chain is ignored in these templates since multiple blockchains support the same script templates
        val P2PKHtemplate = BCHscript(ChainSelector.BCHMAINNET, OP.DUP, OP.HASH160, OP.TMPL_PUBKEYHASH, OP.EQUALVERIFY, OP.CHECKSIG)
        val P2SHtemplate = BCHscript(ChainSelector.BCHMAINNET, OP.HASH160, OP.TMPL_PUBKEYHASH, OP.EQUAL)
    }
}

