package bitcoinunlimited.libbitcoincash

private fun longToUInt32ByteArray(value: Long): ByteArray {
    val bytes = ByteArray(4)
    bytes[3] = (value and 0xFFFF).toByte()
    bytes[2] = ((value ushr 8) and 0xFFFF).toByte()
    bytes[1] = ((value ushr 16) and 0xFFFF).toByte()
    bytes[0] = ((value ushr 24) and 0xFFFF).toByte()
    return bytes
}

class TwoOptionVote {
    companion object {

        @JvmStatic
        fun hash160_salted(salt: ByteArray, buffer: ByteArray): ByteArray {
            return Hash.hash160(salt + buffer)
        }

        @JvmStatic
        fun calculate_proposal_id(
            salt: ByteArray,
            description: String,
            optA: String,
            optB: String,
            endheight: Int,
            participants: Array<ByteArray>
        ): ByteArray {
            var participantsCombined = ByteArray(0)

            for (p in sortByteArray(participants)) {
                participantsCombined = participantsCombined + p
            }
            var proposalIDParts = arrayOf(
                salt,
                description.toByteArray(),
                optA.toByteArray(),
                optB.toByteArray(),
                longToUInt32ByteArray(endheight.toLong()),
                participantsCombined
            )

            var proposalID = ByteArray(0)
            for (p in proposalIDParts) {
                proposalID = proposalID + p
            }

            return Hash.hash160(proposalID)
        }

        @JvmStatic
        fun deriveContractAddress(
            blockchain: ChainSelector,
            proposalID: ByteArray,
            optA: ByteArray,
            optB: ByteArray,
            voterPKH: ByteArray
        ): PayAddress {

            var hash = Hash.hash160(redeemScript(proposalID, optA, optB, voterPKH))
            return PayAddress(blockchain, PayAddressType.P2SH, hash)
        }

        /**
         * Get the full redeemscript.
         */
        @JvmStatic
        fun redeemScript(
            proposalID: ByteArray,
            optA: ByteArray,
            optB: ByteArray,
            voterPKH: ByteArray
        ): ByteArray {
            val scriptHex = (
                "5479a988547a5479ad557a5579557abb537901147f75537a887b01147f77" +
                    "767b8778537a879b7c14beefffffffffffffffffffffffffffffffff" +
                    "ffff879b"
                )
            val script = UtilStringEncoding.hexToByteArray(scriptHex)

            return (
                OP.push(proposalID) +
                    OP.push(optB) +
                    OP.push(optA) +
                    OP.push(voterPKH) +
                    script
                )
        }

        @JvmStatic
        fun createVoteMessage(
            proposalID: ByteArray,
            option: ByteArray
        ): ByteArray {
            if (proposalID.size != 20) {
                error("Invalid proposalID length")
            }
            if (option.size != 20) {
                error("Invalid option length")
            }
            return proposalID + option
        }

        @JvmStatic
        fun signVoteMessage(
            secret: ByteArray,
            message: ByteArray
        ): ByteArray {
            if (message.size != 40) {
                error("Invalid message length")
            }
            return Key.signDataUsingSchnorr(message, secret)
        }

        /**
         * Sort array of bytearray.
         *
         * Workaround for .sort() throwing "byte[] cannot be cast to java.lang.Comparable".
         */
        @JvmStatic
        fun sortByteArray(array: Array<ByteArray>): Array<ByteArray> {
            var asStr: MutableList<String> = mutableListOf()
            for (e in array) {
                asStr.add(UtilStringEncoding.toHexString(e))
            }
            asStr.sort()
            var sorted: MutableList<ByteArray> = mutableListOf()
            for (e in asStr) {
                sorted.add(UtilStringEncoding.hexToByteArray(e))
            }
            return sorted.toTypedArray()
        }
    }
}
