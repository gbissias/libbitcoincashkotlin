// Copyright (c) 2020 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash

import java.util.*
import kotlin.reflect.*
import kotlin.reflect.full.*

val MAX_SHOW_COLLECTION_SIZE = 5

enum class Display(val v: Int)
{
    Simple(1), User(2), Dev(3), None(9999)
}

enum class DisplayStyle(val v: Int)
{
    FieldPerLine(1), ObjectPerLine(2), OneLine(3),
    Help(4), NoTypes(5), ShortTypes(6)
}

typealias DisplayStyles = EnumSet<DisplayStyle>

val DEFAULT_STYLE = EnumSet.of(DisplayStyle.FieldPerLine)

/** indicates that a member field or function should be displayed in the command line interface
@param display Provides a user "capability" level to indicate more esoteric or dangerous options
@param help Provides a short description of the field
@param delve Should this member be displayed recursively? If delve > recursive depth, then the field will be fully shown
 */
annotation class cli(val display: Display, val help: String, val delve: Int = 0)

fun show(p: KProperty1<*, *>, obj: Any): String
{
    if (p.returnType.classifier == MutableMap::class)
    {
        val sz = (p.getter.call(obj) as MutableMap<*, *>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as MutableMap<*, *>).size} elements }"
    }
    if (p.returnType.classifier == Map::class)
    {
        val sz = (p.getter.call(obj) as Map<*, *>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as Map<*, *>).size} elements }"
    }
    if (p.returnType.classifier == MutableList::class)
    {
        val sz = (p.getter.call(obj) as MutableList<*>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as MutableList<*>).size} elements }"
    }
    if (p.returnType.classifier == List::class)
    {
        val sz = (p.getter.call(obj) as List<*>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as List<*>).size} elements }"
    }
    if (p.returnType.classifier == MutableSet::class)
    {
        val sz = (p.getter.call(obj) as MutableSet<*>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as MutableSet<*>).size} elements }"
    }
    if (p.returnType.classifier == Set::class)
    {
        val sz = (p.getter.call(obj) as Set<*>).size
        if (sz > MAX_SHOW_COLLECTION_SIZE)
            return "{ ${(p.getter.call(obj) as Set<*>).size} elements }"
    }

    return p.getter.call(obj).toString()
}

/** Shows or doesn't show the help and formats it depending on the display style */
fun showHelp(s: String, style: DisplayStyles, indent: Int): String
{
    if (!style.contains(DisplayStyle.Help)) return ""
    if (style.contains(DisplayStyle.FieldPerLine)) return "\n" + "  ".repeat(indent+1) + "description: " + s
    else return "  (" + s + ")"
}


fun showProperties(obj: Any, cls: KClass<*>, level: Display, style: DisplayStyles, depth: Int = 0, indent: Int = 0): String
{
    val fieldSep = if (style.contains(DisplayStyle.OneLine)) ", " else "\n"

    val ret = StringBuilder()
    for (p in cls.declaredMemberProperties)
    {
        val d = p.findAnnotation<cli>()
        if (d != null)
        {
            if (d.display.v <= level.v)
            {
                //val pcls = p.getter.call(obj)!!::class
                val pcls = p.returnType.classifier
                // p.returnType.withNullability(false).findAnnotation<cli>()
                // p.returnType.withNullability(false).classifier!!
                if ((pcls != null) && ((pcls as KClass<*>).findAnnotation<cli>() == null))
                {
                    val v = (pcls as KClass<*>).declaredMemberExtensionFunctions
                    var value = show(p, obj)
                    if (value.length > 0) value = " = " + value
                    ret.append("  ".repeat(indent) + p.name + showType(p.returnType.toString(), style) + value + showHelp(d.help, style, indent + 1) + fieldSep)
                }
                else
                {
                    p.getter.call(obj)?.let {
                        ret.append("  ".repeat(indent) + p.name + showHelp(d.help, style, indent + 1) + ": " + showObject(it,
                            level,
                            style,
                            depth - 1,
                            indent + 1) + fieldSep)
                    }
                }
            }
        }
    }
    return ret.toString()
}


fun <K, V> MutableMap<K, V>.show(level: Display = Display.User, style: DisplayStyles = DEFAULT_STYLE, indent: Int = 0): String?
{
    return "{ ${this.size} elements }"
}

/*
fun<K,V> Map<K,V>.show(level: Display = Display.User, style: DisplayStyle = DisplayStyle.FieldPerLine, indent:Int = 0): String?
{
    return "{ ${this.size} elements }"
}
*/

fun showType(typeName: String?, style: DisplayStyles = DEFAULT_STYLE): String
{
    if (style.contains(DisplayStyle.NoTypes)) return ""
    if (typeName != null)
    {
        if (!style.contains(DisplayStyle.ShortTypes))
        {
            return ": " + typeName
        }
        else
        {
            // short types translates like this:  module.foo<something.bar>  -> foo
            return ": " + typeName.split('<').first().split('.').last()
        }
    }
    return ""
}

fun getObjectType(obj: Any, style: DisplayStyles = DEFAULT_STYLE): String = showType(obj::class.qualifiedName)


fun showObject(
    obj: Any,
    level: Display = Display.User,
    style: DisplayStyles = DEFAULT_STYLE,
    depth: Int = 0,
    indent: Int = 0
): String
{
    var idt = indent
    val ret = StringBuilder()
    val fieldSep = if (style.contains(DisplayStyle.OneLine)) ", " else "\n"
    val fieldPerNewLine = if (style.contains(DisplayStyle.FieldPerLine)) "\n" else ""
    val objectSep = if (style.contains(DisplayStyle.ObjectPerLine) || style.contains(DisplayStyle.FieldPerLine)) "\n" else ", "
    val objectOpener = if (style.contains(DisplayStyle.OneLine)) "{" else "\n"
    val objectCloser = if (style.contains(DisplayStyle.OneLine)) "}" else ""

    for (st in obj::class.allSuperclasses)
    {
        val superProps = showProperties(obj, st, level, style, depth - 1, idt + 1)
        if (superProps.length > 0)  // Don't display the name of a superclass that has no properties of interest
        {
            ret.append("  ".repeat(idt) + st.qualifiedName + fieldSep)
            ret.append(superProps + fieldSep)
        }
    }

    /* Displays the object's type, but that is now displayed earlier
    val typeName = obj::class.qualifiedName
    if (typeName != null)
    {
        if (style.contains(DisplayStyle.FieldPerLine))
        {
            ret.append("  ".repeat(idt) + "type: " + typeName + fieldSep)
        }
        else
        {
            ret.append(":_ " + typeName.split('.').last())
        }
    } */

    if (style.contains(DisplayStyle.FieldPerLine)) idt += 1
    //if (!style.contains(DisplayStyle.OneLine)) ret.append("\n")

    // Handle annotations in the primary constructor
    obj::class.primaryConstructor?.let { primaryCtor ->
        val vplist: List<KParameter> = primaryCtor.valueParameters
        for (kp in vplist)
        {
            val d = kp.findAnnotation<cli>()
            if (d != null)
            {
                if (d.display.v <= level.v)
                {
                    var value = ""  // TODO get a value from a KParameter
                    //value = obj.javaClass.getField(kp.name).get(obj).toString()  (doesn't find it)
                    //if (value.length != 0) value = " = " + value
                    ret.append("  ".repeat(idt) + kp.name + showType(kp.type.toString(), style) + value + showHelp(d.help, style, indent + 1) + objectSep)
                }
            }
        }
    }

    for (p in obj::class.declaredMemberProperties)
    {
        //println("declaredMemberProperty: " + p.name)
        val d = p.findAnnotation<cli>()
        if (d != null)
        {
            if (d.display.v <= level.v)
            {
                val subObj = p.getter.call(obj)
                // decide whether to display details of subobjects
                if ((subObj != null) && (d.delve + depth > 0))
                    ret.append("  ".repeat(idt) + p.name + getObjectType(subObj, style)
                        + showHelp(d.help, style, indent + 1)
                        + showObject(subObj, level, style, depth - 1, idt + 1) + objectSep)
                // or its summary
                else ret.append("  ".repeat(idt) + p.name + " = " + subObj + fieldSep)
            }
        }
    }

    if (ret.length > 0)  // If the object has contents, wrap it with {} in one line mode
    {
        ret.append(objectCloser);
        ret.insert(0, objectOpener);
    }
    return ret.toString()
}

fun cliDump(
    obj: Any?,
    level: Display = Display.User,
    style: DisplayStyles = DEFAULT_STYLE,
    depth: Int = 2,
    indent: Int = 0
): String
{
    if (obj == null) return "(null)"
    return (showObject(obj, level, style, depth, indent) ?: "") + "\n[" + (getApis(obj, level, EnumSet.of(DisplayStyle.OneLine))
        ?: "") + "]\n"
}

fun shouldDisplay(obj: KAnnotatedElement, level: Display): Boolean
{
    val d = obj.findAnnotation<cli>() ?: return false
    return (d.display.v <= level.v)
}

fun getClassApis(cls: KClass<*>, level: Display = Display.User, style: DisplayStyles = EnumSet.of(DisplayStyle.OneLine)): String?
{
    var uniqueApis = mutableSetOf<String>()
    for (fn in cls.declaredMemberFunctions)
    {
        if (!shouldDisplay(fn, level)) continue
        uniqueApis.add(fn.name)
    }
    for (fn in cls.declaredMemberExtensionFunctions)
    {
        if (!shouldDisplay(fn, level)) continue
        uniqueApis.add(fn.name)
    }
    if (uniqueApis.size == 0) return null
    return uniqueApis.toSortedSet().toList().joinToString(", ")
}

fun getApis(obj: Any, level: Display = Display.User, style: DisplayStyles = EnumSet.of(DisplayStyle.OneLine)): String?
{
    val ret = mutableListOf<String>()
    for (st in obj::class.allSuperclasses)
    {
        if (!shouldDisplay(st, level)) continue
        getClassApis(st, level, style)?.let { ret.add(it) }
    }
    getClassApis(obj::class, level, style)?.let { ret.add(it) }
    if (ret.size == 0) return null
    return ret.joinToString(", ")
}