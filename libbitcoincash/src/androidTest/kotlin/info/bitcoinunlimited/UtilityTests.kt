package info.bitcoinunlimited.libbitcoincash

import android.util.Log
import bitcoinunlimited.libbitcoincash.*
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import kotlin.test.assertEquals
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Hash
import bitcoinunlimited.libbitcoincash.Initialize
import bitcoinunlimited.libbitcoincash.UtilStringEncoding
import com.google.common.truth.Truth
import kotlinx.coroutines.*
import org.junit.Assert
import java.math.BigInteger
import java.util.*
import java.util.concurrent.Executors
import java.util.logging.Logger
import kotlin.coroutines.CoroutineContext

val LogIt = Logger.getLogger("AndroidTest")

// assert is sometimes compiled "off" but in tests we never want to skip the checks so create a helper function
fun check(v: Boolean?)
{
    if (v==null) throw AssertionError("check failed")
    if (!v) throw AssertionError("check failed")
}

class UtilityTests
{
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }

    @Test
    fun testCodec()
    {
        Log.e("testCodec", "testCodec start")
        var data = byteArrayOf(1,2,3,4,5,6,7,8,9,10, 0, -1, 127, -127 )
        var str = Codec.encode64(data)
        Log.e("testCodec", str)
        var data2 = Codec.decode64(str)
        check(data contentEquals data2)
    }

    @Test
    fun testCoCond()
    {
        val coCtxt: CoroutineContext = Executors.newFixedThreadPool(2).asCoroutineDispatcher()
        val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)

        runBlocking {
            var c1 = CoCond<Nothing?>(coScope)
            var v = 1;
            val cor = GlobalScope.launch { c1.yield(); v = 3 }
            c1.wake(null)
            cor.join()
            Assert.assertEquals(v,3)
        }

        runBlocking {

            var c1 = CoCond<Int>(coScope)
            var v = 1
            val cor = GlobalScope.launch { v = c1.yield()!!; }
            //val cor2 = GlobalScope.launch { v = c1.yield()!!; }
            c1.wake(3)
            //c1.wake(4)
            cor.join()
            //cor2.join()
            check(v == 3 || v == 4)
        }
    }

    @Test
    fun testConverters()
    {
        // Test big integer conversion
        val bic = BigIntegerConverters()
        val test = 12345678.toBigInteger()
        Assert.assertEquals(bic.fromByteArray(bic.toByteArray(test)), test)

        val rnd = Random()
        for (i in 1..1000)
        {
            val bi = BigInteger(256, rnd)
            Assert.assertEquals(bic.fromByteArray(bic.toByteArray(bi)), bi)
        }

        // Test Hash256 converter
        val hac = Hash256Converters()
        for (i:Int in 1..1000)
        {
            val b = byteArrayOf(i.and(255).toByte(), ((i shr 8) and 255).toByte(), ((i shr 16) and 255).toByte())
            val v = Hash256(Hash.sha256(b))
            Assert.assertEquals(hac.fromByteArray(hac.toByteArray(v)), v)
        }
    }

}