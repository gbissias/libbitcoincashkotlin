package info.bitcoinunlimited.libbitcoincash

import bitcoinunlimited.libbitcoincash.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import org.junit.Test
import java.math.BigDecimal
import java.util.concurrent.Executors
import java.util.logging.Logger
import kotlin.coroutines.CoroutineContext


class P2pProtocolTests
{
    companion object
    {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHREGTEST.v)
        }
    }

    @Test
    fun connectToP2P()
    {
        LogIt.info("This test requires a bitcoind full node running on regtest at ${EMULATOR_HOST_IP}:${BCHregtestPort}")

        val coCtxt: CoroutineContext = Executors.newFixedThreadPool(2).asCoroutineDispatcher()
        val coScope: CoroutineScope = kotlinx.coroutines.CoroutineScope(coCtxt)
        val coCond = CoCond<Boolean>(coScope)

        var cnxn = BCHp2pClient(ChainSelector.BCHREGTEST, EMULATOR_HOST_IP, BCHregtestPort, "regtest@${EMULATOR_HOST_IP}",coScope,coCond).connect()
        GlobalScope.launch { cnxn.processForever() }

        cnxn.waitForReady()

        cnxn.send(NetMsgType.PING, BCHserialized(SerializationType.NETWORK) + 1L)

        val loc = BlockLocator()
        loc.add(Hash256())  // This will ask for the genesis block because no hashes will match
        val stop = Hash256()
        var headers: MutableList<BlockHeader>? = null
        val waiter = ThreadCond()
        cnxn.getHeaders(loc, stop, { lst, _  -> headers = lst; waiter.wake(); true})

        waiter.delay(5000)
        check(headers != null)  // If its null we didn't get the headers

        for (hdr in headers!!)
        {
            LogIt.info(hdr.height.toString() + " hash: " + hdr.hash.toHex() + " prev: " + hdr.hashPrevBlock.toHex())
        }


        LogIt.info("shutting down")
        cnxn.close()
        LogIt.info("TestCompleted")
    }
}