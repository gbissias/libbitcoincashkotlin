package info.bitcoinunlimited.libbitcoincash

import bitcoinunlimited.libbitcoincash.BCHinput
import bitcoinunlimited.libbitcoincash.BCHoutpoint
import bitcoinunlimited.libbitcoincash.BCHtransaction
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Hash
import bitcoinunlimited.libbitcoincash.Initialize
import bitcoinunlimited.libbitcoincash.Pay2PubKeyHashDestination
import bitcoinunlimited.libbitcoincash.PayAddress
import bitcoinunlimited.libbitcoincash.PayAddressType
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.SerializationType
import bitcoinunlimited.libbitcoincash.TwoOptionVoteContract
import com.google.common.truth.Truth.assertThat
import info.bitcoinunlimited.VoteTestUtil
import kotlin.test.assertFailsWith
import org.junit.Test

fun toPKH(pubkey: ByteArray): ByteArray {
    return Hash.hash160(pubkey)
}

fun p2pkhAddress(chain: ChainSelector, privateKey: ByteArray): PayAddress {
    val pkh = toPKH(PayDestination.GetPubKey(privateKey))
    return PayAddress(chain, PayAddressType.P2PKH, pkh)
}

fun mocInput(chain: ChainSelector, privateKey: ByteArray, amount: Long): BCHinput {
    var input = BCHinput(chain)
    input.spendable.priorOutScript = p2pkhAddress(chain, privateKey).outputScript()
    input.spendable.amount = amount
    input.spendable.secret = privateKey
    return input
}

fun outputToInput(
    chain: ChainSelector,
    tx: BCHtransaction,
    outputIndex: Int,
    secret: ByteArray
): BCHinput {
    var input = BCHinput(chain)
    input.spendable.amount = tx.outputs[outputIndex.toInt()].amount
    input.spendable.outpoint = BCHoutpoint(tx.calcHash(), outputIndex)
    input.spendable.secret = secret
    return input
}

class TwoOptionVoteContractTests {
    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }

    var allContracts: List<TwoOptionVoteContract> = VoteTestUtil.contractInstances()
    var chain: ChainSelector = ChainSelector.BCHMAINNET
    var privateKey: ByteArray = VoteTestUtil.mocPrivateKey()
    var changeAddress: PayDestination = Pay2PubKeyHashDestination(
        ChainSelector.BCHMAINNET, VoteTestUtil.mocPrivateKey('B')
    )

    /**
     * Test that when input is enough, but low, that all coins are sent to the
     * contract.
     */
    @Test
    fun fundWithOneOutput() {
        val inputAmount = (
            -1 + TwoOptionVoteContract.MIN_CONTRACT_INPUT +
                TwoOptionVoteContract.MIN_CHANGE_OUTPUT +
                TwoOptionVoteContract.FUND_FEE
            )

        val contract = this.allContracts.get(0)
        val coin = mocInput(this.chain, this.privateKey, inputAmount)
        val tx = contract.fundContract(this.chain, coin, this.changeAddress)
        assertThat(tx.outputs.size).isEqualTo(1)
        assertThat(tx.outputs[0].amount).isEqualTo(inputAmount - TwoOptionVoteContract.FUND_FEE)
        assertThat(tx.outputs[0].script.flatten())
            .isEqualTo(contract.outputScript(this.chain).flatten())
        assertThat(tx.inputs.size).isEqualTo(1)
    }

    /**
     * Test that change output is created when input is larger than MIN_CONTRACT_INPUT
     */
    @Test
    fun fundWithChangeOutput() {
        val inputAmount = (
            TwoOptionVoteContract.MIN_CONTRACT_INPUT +
                TwoOptionVoteContract.MIN_CHANGE_OUTPUT +
                TwoOptionVoteContract.FUND_FEE
            )

        val contract = this.allContracts.get(0)
        val coin = mocInput(this.chain, this.privateKey, inputAmount)
        val tx = contract.fundContract(this.chain, coin, this.changeAddress)
        assertThat(tx.outputs.size).isEqualTo(2)
        assertThat(tx.outputs[0].amount).isEqualTo(TwoOptionVoteContract.MIN_CONTRACT_INPUT)
        assertThat(tx.outputs[0].script.flatten())
            .isEqualTo(contract.outputScript(this.chain).flatten())
        assertThat(tx.outputs[1].amount).isEqualTo(TwoOptionVoteContract.MIN_CHANGE_OUTPUT)
        assertThat(tx.inputs.size).isEqualTo(1)
    }

    /**
     * Test that exception is thrown when input is too small.
     */
    @Test
    fun fundTooLowInput() {
        val contract = this.allContracts.get(0)
        val inputAmount = (
            TwoOptionVoteContract.MIN_CONTRACT_INPUT +
                TwoOptionVoteContract.FUND_FEE - 1
            )

        // IllegalArgumentException beause input is too low.
        assertFailsWith(IllegalArgumentException::class) {
            val coin = mocInput(this.chain, this.privateKey, inputAmount)
            contract.fundContract(chain, coin, changeAddress)
        }
    }

    /**
     * Test that exception is thrown when invalid option for contract is passed.
     */
    @Test
    fun castInvalidOption() {
        val contract = this.allContracts.get(0)
        val invalidOption = ByteArray(20, { _ -> 'F'.toByte() })

        assertFailsWith(IllegalArgumentException::class) {
            val coin = mocInput(this.chain, this.privateKey, 100)
            contract.castVote(this.chain, coin, this.changeAddress, invalidOption)
        }
    }

    /**
     * Test casting a valid vote.
     */
    @Test
    fun castVoteTest() {
        val contract = this.allContracts.get(0)

        val inputAmount = (
            TwoOptionVoteContract.MIN_CONTRACT_INPUT +
                TwoOptionVoteContract.FUND_FEE
            )

        val fundCoin = mocInput(this.chain, this.privateKey, inputAmount)
        val fundTx = contract.fundContract(
            this.chain, fundCoin,
            this.changeAddress
        )

        val contractCoin = outputToInput(this.chain, fundTx, 0, this.privateKey)
        val castTx = contract.castVote(
            this.chain, contractCoin,
            this.changeAddress,
            TwoOptionVoteContract.BLANK_VOTE
        )

        assertThat(castTx.inputs.size).isEqualTo(1)
        assertThat(castTx.inputs[0].BCHserialize(SerializationType.HASH).flatten())
            .isEqualTo(contractCoin.BCHserialize(SerializationType.HASH).flatten())
        assertThat(castTx.outputs.size).isEqualTo(1)
        assertThat(castTx.outputs[0].amount).isEqualTo(
            inputAmount - TwoOptionVoteContract.FUND_FEE - TwoOptionVoteContract.CAST_FEE
        )
    }

    @Test
    fun testContractAddress() {
        var salt = "unittest".toByteArray()
        var chain = ChainSelector.BCHMAINNET

        val contract = VoteTestUtil.contractInstances()

        assertThat(contract[0].address(chain).toString())
            .isEqualTo("bitcoincash:prx7fmgt7rav309qxjnxdw6zhran36t9dcvpyhsw4j")
        assertThat(contract[1].address(chain).toString())
            .isEqualTo("bitcoincash:pqj48avrnj5wzshf9ye7rqu5m3v3wj3cj5439m3w0d")
        assertThat(contract[2].address(chain).toString())
            .isEqualTo("bitcoincash:pzrpg8yrvrwsqkk8rzwu4mjncmcxgt0spv2pjfxcfs")
    }
}
