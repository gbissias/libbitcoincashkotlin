package info.bitcoinunlimited.libbitcoincash

import bitcoinunlimited.libbitcoincash.*
import kotlinx.serialization.Serializable
import org.junit.Test

val SimulationHostIP = "10.0.2.2"
// The IP address of the host machine: Android sets up a fake network with the host hardcoded to this IP
val EMULATOR_HOST_IP = SimulationHostIP //"192.168.1.100" //"10.0.2.2"

class ElectrumProtocolTests
{
     companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }

    @Test
    fun testelectrumclient()
    {
        LogIt.info("This test requires an electrum cash server running on regtest at ${EMULATOR_HOST_IP}:${DEFAULT_TCP_ELECTRUM_PORT_REGTEST}")
        val cnxn = try
        {
            ElectrumClient(ChainSelector.BCHREGTEST,
                EMULATOR_HOST_IP,
                DEFAULT_TCP_ELECTRUM_PORT_REGTEST,
                "Electrum@${EMULATOR_HOST_IP}:${DEFAULT_TCP_ELECTRUM_PORT_REGTEST}")
        }
        catch(e: Exception)
        {
            LogIt.warning("Exception: " + e.toString())
            LogIt.warning(e.stackTraceToString())
            throw e
        }
        cnxn.start()

        val ret = cnxn.call("server.version", listOf("4.0.1", "1.4"), 1000)
        if (ret!=null) LogIt.info(sourceLoc() + ": Server Version returned: " + ret)

        val version = cnxn.version()
        LogIt.info(sourceLoc() + ": Version API call returned: " + version.first + " " + version.second)

        // TODO enable when electrscash is updated
        val features = cnxn.features()
        LogIt.info(sourceLoc() + ": genesis block hash:" + features.genesis_hash)
        check("0f9188f13cb7b2c71f2a335e3a4fc328bf5beb436012afca590b1a11466e2206" == features.genesis_hash)
        check("sha256" == features.hash_function)
        check(features.server_version.contains("ElectrsCash"))  // Clearly this may fail if you connect a different server to this regression test

        val ret2 = cnxn.call("blockchain.block.header", listOf(100, 102), 1000)
        LogIt.info(ret2)

        try {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d00", 5000)  // doesn't exist
            assert(false)
        }
        catch(e: ElectrumIncorrectRequest)
        {
            if (e.message!!.contains("still syncing"))
            {
                LogIt.warning("This test is more effective if you generate 101 blocks on regtest")
            }
            else assert(e.message!!.contains("not indexed tx"))
        }

        try
        {
            cnxn.getTx("zz5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d", 5000) // bad hash
            assert(false)
        }
        catch(e: ElectrumIncorrectRequest)
        {
        }

        try
        {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d", 5000) // bad hash (short)
            assert(false)
        }
        catch(e: ElectrumIncorrectRequest)
        {
        }

        try
        {
            cnxn.getTx("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d".repeat(10), 1000) // bad hash (large)
            assert(false)
        }
        catch(e: ElectrumIncorrectRequest)
        {
        }

        val (name, ver) = cnxn.version(1000)
        LogIt.info("Server name $name, server version $ver")

        cnxn.call("server.banner", null) {
            LogIt.info("Server Banner reply is: " + it)
        }

        @Serializable
        data class BannerReply(val result: String)
        cnxn.parse("server.banner", null, BannerReply.serializer() ) {
            LogIt.info("Server Banner reply is: " + it!!.result)
        }


        cnxn.subscribe("blockchain.headers.subscribe") {
            LogIt.info("Received blockchain header notification: ${it}")
        }

        val header = cnxn.getHeader(10000000)  // beyond the tip
        LogIt.info(header.toString())

        try
        {
            cnxn.getHeader(-1)  // beyond the tip
        }
        catch (e: ElectrumIncorrectRequest)
        {
            LogIt.info(e.toString())
        }
        LogIt.info(header.toString())


        // This code gets the first coinbase transaction and then checks its history.  Based on the normal regtest generation setup, there should be at least 100
        // blocks that generate to this same output.
        val tx = cnxn.getTxAt(1, 0)
        LogIt.info(tx.toHex())
        tx.debugDump()

        val txBlkHeader = BlockHeader(BCHserialized(cnxn.getHeader(1), SerializationType.HASH))

        // TODO add in when get_first_use is committed to electrscash.  TODO: check server capabilities
        val firstUse = cnxn.getFirstUse(tx.outputs[0].script)
        LogIt.info("first use in block ${firstUse.block_hash}:${firstUse.block_height}, transaction ${firstUse.tx_hash}")
        check(firstUse.tx_hash != null)
        check(firstUse.block_height!! == 1)
        check(tx.hash.toHex() == firstUse.tx_hash!!)
        check(txBlkHeader.hash.toHex() == firstUse.block_hash)

        // doesn't exist
        try
        {
            val firstUse2 = cnxn.getFirstUse("5a2e45c999509a3505cf543d462977b198957abefcc9c86f4a0ef59525363d00")
            assert(false)
        }
        catch(e: ElectrumNotFound)
        {
            // expected
        }

        val uses = cnxn.getHistory(tx.outputs[0].script)
        for (use in uses)
        {
            LogIt.info("used in block ${use.first} tx ${use.second}")
        }

        assert(uses.size >= 100)  // Might be wrong if the regtest chain startup is changed.

        val headers = cnxn.getHeaders(0, 1000, 10000)
        for (i in headers.indices)
        {
            val hdr = cnxn.getHeader(i, 1000)
            check(hdr contentEquals headers[i])

        }

        //Thread.sleep(20000)
        cnxn.close()
    }
}