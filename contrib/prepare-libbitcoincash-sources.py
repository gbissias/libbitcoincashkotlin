#!/usr/bin/env python3
from pathlib import Path
import logging
import os
import sys
import tarfile
import urllib.request
import zipfile

logging.basicConfig(stream = sys.stdout, level = logging.DEBUG)
logger = logging.getLogger("build")

ROOT_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "build")


BU_RELEASE = "https://gitlab.com/bitcoinunlimited/BCHUnlimited/-/archive/dev/BCHUnlimited-dev.zip"
BU_ARCHIVE_NAME = "bu.zip"
BU_ARCHIVE_PATH = os.path.join(ROOT_DIR, BU_ARCHIVE_NAME)
BU_SRC_PATH = os.path.join(ROOT_DIR, "BU")
BU_ROOT_DIR = os.path.join(BU_SRC_PATH, "BCHUnlimited-dev")

CASHLIB_PATH = os.path.join(BU_ROOT_DIR, "src/cashlib")

BOOST_RELEASE = "https://dl.bintray.com/boostorg/release/1.70.0/source/boost_1_70_0.tar.bz2"
BOOST_ARCHIVE_NAME = "boost.tar.bz2"
BOOST_ARCHIVE_PATH = os.path.join(ROOT_DIR, BOOST_ARCHIVE_NAME)
BOOST_SRC_PATH = os.path.join(CASHLIB_PATH, "boost_1_70_0")

# Fake User-Agent, urllib gets 403 forbidden from Gitlab
opener = urllib.request.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0')]
urllib.request.install_opener(opener)

def download_bu():
    if os.path.exists(os.path.join(CASHLIB_PATH, "CMakeLists.txt")):
        logger.info(f"Using existing BU source at {BU_SRC_PATH}")
        return

    if not os.path.exists(BU_ARCHIVE_PATH):
        logger.info(f"Fetching Bitcoin Unlimited from {BU_RELEASE} to {BU_ARCHIVE_PATH}")
        urllib.request.urlretrieve(BU_RELEASE, BU_ARCHIVE_PATH)
    else:
        logger.info(f"Using existing Bitcoin Unlimited download {BU_ARCHIVE_PATH}")

    logger.info(f"Extracting {BU_ARCHIVE_NAME} to {BU_SRC_PATH}")
    with zipfile.ZipFile(BU_ARCHIVE_PATH, 'r') as fh:
        fh.extractall(BU_SRC_PATH)

def download_boost():
    if not os.path.exists(CASHLIB_PATH):
        raise Exception("{CASHLIB_PATH} does not exist")

    if os.path.exists(BOOST_SRC_PATH):
        logger.info(f"Using existing boost source at {BOOST_SRC_PATH}")
        return

    if not os.path.exists(BOOST_ARCHIVE_PATH):
        logger.info(f"Fetching boost from {BOOST_RELEASE} to {BOOST_ARCHIVE_PATH}")
        urllib.request.urlretrieve(BOOST_RELEASE, BOOST_ARCHIVE_PATH)
    else:
        logger.info(f"Using existing boost download {BOOST_ARCHIVE_PATH}")

    logger.info(f"Extracting headers from {BOOST_ARCHIVE_NAME} to {BOOST_SRC_PATH}")
    tar = tarfile.open(BOOST_ARCHIVE_PATH, "r:bz2")

    def is_header(t):
        return t.name[-4:] == ".hpp" \
            or t.name[-4:] == ".ipp" \
            or t.name[-2:] == ".h"

    headers = [ f for f in tar.getmembers() if is_header(f) ]

    tar.extractall(path = CASHLIB_PATH, members = headers)

def output_reader(pipe, queue):
    try:
        with pipe:
            for l in iter(pipe.readline, b''):
                queue.put(l)
    finally:
        queue.put(None)

def run_cmd(*, cmd, args, cwd, env = None):
    import subprocess
    from threading import Thread
    from queue import Queue

    args = [cmd] + args
    logger.info("Running %s", args)

    p = subprocess.Popen(args, cwd = cwd,
        stdout = subprocess.PIPE, stderr = subprocess.PIPE, env = env)

    q = Queue()
    Thread(target = output_reader, args = [p.stdout, q]).start()
    Thread(target = output_reader, args = [p.stderr, q]).start()

    for line in iter(q.get, None):
        logger.info(line.decode('utf-8').rstrip())

    p.wait()
    rc = p.returncode
    assert rc is not None
    if rc != 0:
        raise Exception(f"{args} failed with return code {rc}")

def run_autogen():
    if os.path.exists(os.path.join(BU_ROOT_DIR, "configure")):
        logger.info(f"Autogen has already been run, skipping")
        return

    # On OSX, when installing autotools with 'brew', autotools are installed
    # in /usr/local/bin
    # Make sure it's in the path
    env = os.environ
    env["PATH"] += ":/usr/local/bin"

    try:
        run_cmd(
            cmd = "/usr/bin/env",
            args = ["sh", "./autogen.sh"],
            cwd = BU_ROOT_DIR,
            env = env)
    except Exception as e:
        logger.warning("'autogen.sh' failed. Are packages `automake` and `libtool` installed?")
        raise e

def bitcoin_config_workaround():
    # bitfield.cpp expects this file to exist
    cfg = os.path.join(BU_ROOT_DIR, "src", "config", "bitcoin-config.h")
    if not os.path.exists(cfg):
        Path(cfg).touch()

def main():
    Path(ROOT_DIR).mkdir(parents = True, exist_ok = True)
    download_bu()
    download_boost()
    run_autogen()
    bitcoin_config_workaround()

main()
